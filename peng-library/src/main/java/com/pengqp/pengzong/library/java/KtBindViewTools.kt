package com.pengqp.pengzong.library.java

import android.app.Activity

class KtBindViewTools {
    companion object {
        fun bind(activity: Activity) {
            val cls = activity.javaClass
            val bindViewClass = Class.forName(cls.name + "_KtViewBinding")
            val method = bindViewClass.getMethod("bind", activity.javaClass)
            method.invoke(bindViewClass.newInstance(), activity)
        }
    }
}