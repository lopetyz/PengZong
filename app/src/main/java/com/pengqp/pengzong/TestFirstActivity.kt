package com.pengqp.pengzong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.pengqp.pengzong.annotation.kotlin.BindKtView
import com.pengqp.pengzong.library.java.KtBindViewTools

class TestFirstActivity : AppCompatActivity() {

    @BindKtView(R.id.text_bind_show)
    var textBindView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_first)

        KtBindViewTools.bind(this)

        textBindView?.text = "hello pengzong"
    }
}