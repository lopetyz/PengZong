package com.pengqp.pengzong

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pengqp.pengzong.annotation.java.HomeRoute
import com.pengqp.pengzong.library.java.HomeRouteTools

@HomeRoute(
    ids = [R.id.btn_first, R.id.btn_second, R.id.btn_third],
    classNames = [
        "com.pengqp.pengzong.TestFirstActivity",
        "com.pengqp.pengzong.TestSecondActivity",
        "com.pengqp.pengzong.TestThirdActivity"
    ]
)
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        HomeRouteTools.launch(this)
    }
}