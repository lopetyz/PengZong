package com.pengqp.pengzong.annotation.kotlin

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.SOURCE)
annotation class BindKtView(val id: Int)