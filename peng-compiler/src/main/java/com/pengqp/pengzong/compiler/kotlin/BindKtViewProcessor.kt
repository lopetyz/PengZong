package com.pengqp.pengzong.compiler.kotlin

import com.google.auto.service.AutoService
import com.pengqp.pengzong.annotation.kotlin.BindKtView
import java.util.*
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.util.Elements
import kotlin.collections.HashMap
import kotlin.collections.HashSet

@AutoService(Processor::class)
class BindKtViewProcessor : AbstractProcessor() {

    private var messager: Messager? = null
    private var elementUtils: Elements? = null
    private val proxyMap = HashMap<String, BindKtViewClassCreatorProxy>()

    override fun init(processingEnv: ProcessingEnvironment?) {
        super.init(processingEnv)
        messager = processingEnv?.messager
        elementUtils = processingEnv?.elementUtils
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return HashSet(Collections.singletonList(BindKtView::class.java.canonicalName))
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment?): Boolean {
        proxyMap.clear()

        roundEnv?.apply {
            for (element in getElementsAnnotatedWith(BindKtView::class.java)) {
                val classElement = element.enclosingElement as TypeElement
                val fullClassName = classElement.qualifiedName.toString()
                var proxyBind: BindKtViewClassCreatorProxy
                if (proxyMap.containsKey(fullClassName) && proxyMap[fullClassName] != null) {
                    proxyBind = proxyMap[fullClassName]!!
                } else {
                    proxyBind = BindKtViewClassCreatorProxy(elementUtils, classElement)
                    proxyMap[fullClassName] = proxyBind
                }
                val bindAnnotation = element.getAnnotation(BindKtView::class.java)
                val id = bindAnnotation.id
                proxyBind.putElement(id, element as VariableElement)

                //生成kt文件
                for (key in proxyMap.keys) {
                    val proxyInfo = proxyMap[key]
                    val file = proxyInfo?.generateFile()
                    file?.writeTo(processingEnv.filer)
                }
            }
        }

        return true
    }
}